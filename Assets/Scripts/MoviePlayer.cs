﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoviePlayer : MonoBehaviour
{
	public string moviePath;
    void Start()
    {
		if (string.IsNullOrEmpty(moviePath)) {
			Debug.LogError("Path could not be found!");
			return;
		}
        // Will attach a VideoPlayer to the main camera.
        GameObject camera = GameObject.Find("Main Camera");

        // VideoPlayer automatically targets the camera backplane when it is added
        // to a camera object, no need to change videoPlayer.targetCamera.
        var videoPlayer = camera.AddComponent<UnityEngine.Video.VideoPlayer>();

        // By default, VideoPlayers added to a camera will use the back plane.
        // Let's target a material instead.
		videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;

		videoPlayer.targetMaterialRenderer = GetComponent<Renderer>();

        // This will cause our scene to be visible through the video being played.
        videoPlayer.targetCameraAlpha = 0.5F;

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute.
        videoPlayer.url = moviePath;

        // Restart from beginning when done.
        videoPlayer.isLooping = true;

        // Each time we reach the end, we slow down the playback by a factor of 10.
        videoPlayer.loopPointReached += (player) => {
			Debug.Log("End Reached");
		};
		videoPlayer.prepareCompleted += (player) => {
			player.Play();
		};
		
        // Start playback. This means the player may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.
        videoPlayer.Prepare();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
