# README

This is a sample project that demonstrates the barebones for a 360 Video VR Application.

Things to note:

- It uses the new VideoPlayer Class (Unity 5.6.0). This class is limited in its decoding capacities see [docs](https://docs.unity3d.com/2017.1/Documentation/ScriptReference/Video.VideoPlayer.html).

- The AudioSources use the in-built Unity spatializer. The example uses sources with no roll off and no effects. Only pure HRTF convolution is desired for static sources. Effects and gain should be applied in advance.